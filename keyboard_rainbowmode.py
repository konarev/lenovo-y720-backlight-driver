#!/usr/bin/env python3
from random import randrange

from pynput import keyboard

import driver

def rand(_):
    driver.set_status(randrange(4), tuple(randrange(24) for i in range(4)))

with keyboard.Listener(on_press=rand, on_release=rand) as listener:
    listener.join()
