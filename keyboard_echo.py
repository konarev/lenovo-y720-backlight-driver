#!/usr/bin/env python3
from random import randrange
from threading import Thread
import time

from pynput import keyboard

import driver

cnt = 0

def echo(_):
    global cnt
    cnt += 1
    thread = Thread(target=echo_thread, args=[cnt])
    thread.start()

def echo_thread(number):
    global cnt
    if (cnt > number):
        return
    driver.set_status(driver.Style.ALWAYS_ON, driver.get_status()["cols"], brightness=[4,4,4,4])
    time.sleep(0.05)
    if (cnt > number):
        return
    driver.set_status(driver.Style.ALWAYS_ON, driver.get_status()["cols"], brightness=[2,2,2,2])
    time.sleep(0.1)
    if (cnt > number):
        return
    driver.set_status(driver.Style.ALWAYS_ON, driver.get_status()["cols"], brightness=[1,1,1,1])
    time.sleep(0.1)
    if (cnt > number):
        return
    driver.set_status(driver.Style.ALWAYS_ON, driver.get_status()["cols"], brightness=[0,0,0,0])

with keyboard.Listener(on_press=echo) as listener:
    listener.join()
